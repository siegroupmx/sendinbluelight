#!/usr/bin/php
<?php
/**
* Login simple
*/

include( "../autoload.php" );

$user= "__USERNAME_MAIL___";
$key= "___KEYTOKEN___";

$mail= new sendinBlue($user, $key);

/**
* set mail from information
*/
$mail->setMailFrom("client@gmail.com", "Pedro Ramirez");
# $mail->setMailFrom("client@gmail.com"); /* only mail from */
# $mail->setMailFromName("Pedro Ramirez"); /* only name from */

/**
* set mail to information
*/
$mail->setMailTo("me@gmail.com", "Angel Cantu");
# $mail->setMailTo("me@gmail.com"); /* only mail to */
# $mail->setMailToName("Angel Cantu"); /* only name to */

$mail->setSubject("Notificacion de Envio..");

$msg= '<html><body>Hola que tal cliente...</body></html>';
$mail->setHtmlContent($msg);

/**
* agregamos adjuntos
*/
$mail->setAttached("imagen.jpg", "http://jobs.sie-group.net/juego.jpg", "url");	/* adjuntando URL */
$mail->setAttached("imagen2.jpg", file_get_contents("http://jobs.sie-group.net/juego.jpg")); /* adjuntando flujo de datos*/

/**
* comprobacion de adjuntos
*/
if( $mail->isAttached() ) {
	$a= $mail->getAttached();
	echo "\n\n---> Adjunto detectados: ". count($a);
	foreach( $a as $key=>$val ) {
		echo "\nNombre: ". $val["name"];
		echo "\nData:\n". $val["content"];
		echo "\n";
	}
}

$mail->send(); /* sending mail */

if( $mail->getError() ) {
	echo "\nError:\n\n";
	echo $mail->getError();
	# print_r($mail->getError("json")); /* error on json */
	# print_r($mail->getError("array")); /* error on array */
}
else {
	echo "\nExito:\n\n";
	$r= $mail->getResponse(); /* response json default */
	# $r= $mail->getResponse("array"); /* on array information response */
	# $r= $mail->getResponse("string"); /* on string information response */

	echo "\n\nExito..\n\n";
	print_r($r);
}
echo "\n\n";
?>