<?php
class sendinBlue{
	const API_SENDINBLUE='https://api.brevo.com/v3/smtp/email';
	const API_SENDINBLUE_STATUS='https://api.brevo.com/v3/smtp/emails';
	const API_SENDINBLUE_CONTACTS='https://api.brevo.com/v3/contacts';
	const API_USER='__SET_DEFAULT_USER__';
	const API_KEY='__SET_DEFAULT_KEY__';
	private $user=NULL;
	private $key=NULL;
	private $error=NULL;
	private $error_code=NULL;
	private $sucess=NULL;
	private $sucess_code=NULL;
	private $email_from=NULL;
	private $email_from_name=NULL;
	private $email_to= array();
	private $email_to_name= array();
	private $email_reply= array();
	private $email_reply_name= array();
	private $email_bcc= array();
	private $email_bcc_name= array();
	private $email_cc= array();
	private $email_cc_name= array();
	private $subject=NULL;
	private $attached=array();
	private $htmlBody=NULL;
	private $headers=NULL;
	private $dataSend=NULL;
	private $headerRequest=NULL;
	private $headerResponse=NULL;
	private $messageId= array();
	private $preventBCCandCC='@moneybox';
	private $contactService=false;
	private $contactArgs=NULL;
	private $newContactMail=NULL;
	private $newContactMailName=NULL;
	private $newContactIdList=NULL;
	private $newListEvents=NULL;

	/**
	* confirma si existen transacciones agregadas
	*
	* @return boolean true si existen o false
	*/
	public function isTransaction() {
		return (count($this->messageId) ? true:false);
	}

	/**
	* establecer el mesageId de una transaccion anterior
	*
	* @param string $a id de la transaccion
	*/
	public function setTransaction($a=NULL) {
		$this->messageId[]= array( "id"=>($a ? $a:NULL), "response"=>array() );
	}

	/**
	* limpia los mesageId registrado
	*/
	public function cleanTransaction() {
		unset($this->messageId);
	}

	/**
	* retorna el mesageId de una transaccion anterior
	*
	* @return string id de la transaccion
	*/
	public function getTransaction() {
		return $this->messageId;
	}

	/**
	* establece adjuntos apartir del stream o url
	*
	* @param string $a steam de datos
	* @param string $t tipo de flujo de datos (url o stream)
	*/
	public function setAttached($n=NULL, $a=NULL, $t="stream") {
		if( $a ) {
			$this->attached[]= array( "name"=>($n ? $n:"no-name"), "content"=>base64_encode(!strcmp($t, "url") ? file_get_contents($a):$a) );
		}
	}

	/**
	* retorna adjuntos en arreglo
	*
	* @return array flujo de datos
	*/
	public function getAttached() {
		return $this->attached;
	}

	/**
	* verifica si existen adjuntos y devuelve falso o verdadero
	*
	* @return boolean falso o verdadero en la existencia de adjuntos
	*/
	public function isAttached() {
		return (count($this->attached) ? true:false);
	}

	/**
	* establece el correo From
	*
	* @param string $a mail from
	* @param string $n mail from name person
	*/
	public function setMailFrom($a=NULL, $n=NULL) {
		$this->email_from= ($a ? $a:NULL);
		$this->email_from_name= ($n ? $n:NULL);
	}

	/**
	* establece el nombre del correo From
	*
	* @param string $a mail from name person
	*/
	public function setMailFromName($a=NULL) {
		$this->email_from_name= ($a ? $a:NULL);
	}

	/**
	* establece el correo To
	*
	* @param string $a mail to
	* @param string $n mail to name person
	*/
	public function setMailTo($a=NULL, $n=NULL) {
		$this->email_to[]= ($a ? $a:NULL);
		$this->email_to_name[]= ($n ? $n:NULL);
	}

	/**
	* establece el nombre del correo To
	*
	* @param string $a mail to name person
	*/
	public function setMailToName($a=NULL) {
		$this->email_to_name= ($a ? $a:NULL);
	}

	/**
	* establece el correo ReplyTo
	*
	* @param string $a mail to
	* @param string $n mail to name person
	*/
	public function setReplyTo($a=NULL, $n=NULL) {
		$this->email_reply[]= ($a ? $a:NULL);
		$this->email_reply_name[]= ($n ? $n:NULL);
	}

	/**
	* establece el nombre del correo ReplyTo
	*
	* @param string $a mail to name person
	*/
	public function setReplyToName($a=NULL) {
		$this->email_reply_name= ($a ? $a:NULL);
	}

	/**
	* establece el correo BCC
	*
	* @param string $a mail to
	* @param string $n mail to name person
	*/
	public function setBccTo($a=NULL, $n=NULL) {
		$this->email_bcc[]= ($a ? $a:NULL);
		$this->email_bcc_name[]= ($n ? $n:NULL);
	}

	/**
	* establece el nombre del correo BCC
	*
	* @param string $a mail to name person
	*/
	public function setBccToName($a=NULL) {
		$this->email_bcc_name= ($a ? $a:NULL);
	}

	/**
	* establece el correo CC
	*
	* @param string $a mail to
	* @param string $n mail to name person
	*/
	public function setCcTo($a=NULL, $n=NULL) {
		$this->email_cc[]= ($a ? $a:NULL);
		$this->email_cc_name[]= ($n ? $n:NULL);
	}

	/**
	* establece el nombre del correo CC
	*
	* @param string $a mail to name person
	*/
	public function setCcToName($a=NULL) {
		$this->email_cc_name= ($a ? $a:NULL);
	}

	/**
	* verifica si existe ReplyTo activo
	* 
	* @return boolean falso o verdadero
	*/
	public function isReplyToActive() {
		return (count($this->email_reply) ? true:false);
	}

	/**
	* verifica si existe BCC activo
	* 
	* @return boolean falso o verdadero
	*/
	public function isBccToActive() {
		return (count($this->email_bcc) ? true:false);
	}

	/**
	* verifica si existe CC activo
	* 
	* @return boolean falso o verdadero
	*/
	public function isCcToActive() {
		return (count($this->email_cc) ? true:false);
	}

	/**
	* establece el asunto
	*
	* @param string $a mail subject
	*/
	public function setSubject($a=NULL) {
		$this->subject= ($a ? $a:NULL);
	}

	/**
	* establece el contenido en html del cuerpo
	*
	* @param string $a mail html body content
	*/
	public function setHtmlContent($a=NULL) {
		$this->htmlBody= ($a ? $a:NULL);
	}

	/**
	* retorna el mail from
	*
	* @return string mail from
	*/
	public function getMailFrom() {
		return $this->email_from;
	}

	/**
	* retorna el nombre del from
	*
	* @return string name from
	*/
	public function getMailFromName() {
		return $this->email_from_name;
	}

	/**
	* retorna el mail to
	*
	* @return array mail to
	*/
	public function getMailTo() {
		$aux=array();
		foreach( $this->email_to as $key=>$val ) {
			$aux[]=array(
				"name"=>($this->email_to_name[$key] ? $this->email_to_name[$key]:NULL), 
				"email"=>$val
			);
		}
		return $aux;
	}

	/**
	* retorna el nombre del to
	*
	* @return string name to
	*/
	public function getMailToName() {
		$aux=array();
		foreach( $this->email_to as $key=>$val ) {
			$aux[]=array(
				"name"=>($this->email_to_name[$key] ? $this->email_to_name[$key]:NULL), 
				"email"=>$val
			);
		}
		return $aux;
	}

	/**
	* retorna el nombre del ReplyTo
	*
	* @return string name replyto
	*/
	public function getReplyToName() {
		$aux=array();
		foreach( $this->email_reply as $key=>$val ) {
			$aux[]=array(
				"name"=>($this->email_reply_name[$key] ? $this->email_reply_name[$key]:NULL), 
				"email"=>$val
			);
		}
		return $aux[0];
	}

	/**
	* retorna el nombre del BCC
	*
	* @return string name bcc
	*/
	public function getBccToName() {
		$aux=array();
		foreach( $this->email_bcc as $key=>$val ) {
			$aux[]=array(
				"name"=>($this->email_bcc_name[$key] ? $this->email_bcc_name[$key]:NULL), 
				"email"=>$val
			);
		}
		return $aux[0];
	}

	/**
	* retorna el nombre del CC
	*
	* @return string name cc
	*/
	public function getCcToName() {
		$aux=array();
		foreach( $this->email_cc as $key=>$val ) {
			$aux[]=array(
				"name"=>($this->email_cc_name[$key] ? $this->email_cc_name[$key]:NULL), 
				"email"=>$val
			);
		}
		return $aux[0];
	}

	/**
	* retorna el mail subject
	*
	* @return string mail subject
	*/
	public function getSubject() {
		return $this->subject;
	}

	/**
	* retorna el mail html content
	*
	* @return string mail html content
	*/
	public function getHtmlContent() {
		return $this->htmlBody;
	}

	/**
	* establece el mensaje de error
	*
	* @param string $a el mensaje de error
	* @param string $c el codigo de error
	*/
	public function setError($a=NULL, $c=NULL) {
		$this->error= ($a ? $a:NULL);
		$this->error_code= ($c ? $c:NULL);
	}

	/**
	* retorna el mensaje de error
	*
	* @param string $a formato de la respuesta (json, array o string)
	*
	* @return string el mensaje de error con codigo
	*/
	public function getError($a="string") {
		$r= array(
			"code"=>$this->error_code, 
			"msg"=>$this->error
		);
		return (!strcmp($a, "json") ? json_encode($r):(!strcmp($a, "array") ? $r:($this->error ? ($this->error_code. ' - '. $this->error):NULL)));		
	}

	/**
	* retorna el codigo de error
	*
	* @return string el codigo de error
	*/
	public function getErrorCode() {
		return $this->error_code;
	}

	/**
	* establece el mensaje de exito
	*
	* @param string $a el mensaje de exito
	* @param string $c el codigo de exito
	*/
	public function setSucess($a=NULL, $c=NULL) {
		$this->sucess= ($a ? $a:NULL);
		$this->sucess_code= ($c ? $c:NULL);
	}

	/**
	* retorna el mensaje de exito
	*
	* @return string el mensaje de exito con codigo
	*/
	public function getSucess() {
		return ($this->sucess_code ? $this->sucess_code. ' - ':'').$this->sucess;
	}

	/**
	* retorna el codigo de exito
	*
	* @return string el codigo de exito
	*/
	public function getSucessCode() {
		return $this->sucess_code;
	}

	/**
	* retorna el mensaje de exito o response en transacciones
	*
	* @param string $a formato de la respuesta (json, array o string)
	* 
	* @return string el mensaje de exito con codigo
	*/
	public function getResponse($a="json") {
		$r= array(
			"code"=>$this->sucess_code, 
			"msg"=>$this->sucess
		);
		return (!strcmp($a, "json") ? json_encode($r):(!strcmp($a, "array") ? $r:($this->sucess_code. ' - '. $this->sucess)));
	}

	/**
	* Retorna contenido de la variable $headerRequest
	*
	* @return string contenido de variable
	*/
	public function getHeaderRequest() {
		return $this->headerRequest;
	}

	/**
	* Establece el contenido del Request
	*
	* @param string contenido de variable
	*/
	public function setHeaderRequest($a=NULL) {
		$this->headerRequest= ($a ? $a:NULL);
	}

	/**
	* Retorna contenido de la variable $headerResponse
	*
	* @return string contenido de variable
	*/
	public function getHeaderResponse() {
		return $this->headerResponse;
	}

	/**
	* Establece el contenido del Response
	*
	* @param string contenido de variable
	*/
	public function setHeaderResponse($a=NULL) {
		$this->headerResponse= ($a ? $a:NULL);
	}

	/**
	* Debugger para analizar la transaccion
	*/
	public function debugResponse() {
		if( !$this->getHeaderResponse() ) # no hay datos
			$this->setError("no se recibio informacion de sendliblue.com", "009");
		else {
			$this->setError(NULL, NULL); /* limpiamos errores */

			// echo "\n\n--- Request: \n";
			// print_r($this->getHeaderRequest());
			// print_r($this->dataSend);
			// echo "\n\n--- Response: \n";
			// print_r($this->getHeaderResponse());

			# $r= explode("\r\n\r\n", $this->getHeaderResponse(), 2);
			# $json= json_decode($r[1]);
			$r= explode("\r\n", $this->getHeaderResponse());
			$json= json_decode($r[(count($r)-1)]);

			// echo "\n\n=====\n";
			// print_r($json);
			// echo "\n=====\n";

			if( $this->isTransaction() ) {
				if( count($json->events) ) {
					$this->newListEvents= $json->events; // guardamos los eventos
				}
				else {
					if( !count($json->count) )
						$this->setError("no hay eventos", "E-01");
					else {
						foreach( $json->transactionalEmails as $k=>$v ) {
							$arr[]= array(
								"mail"=>$v->email, 
								"messageId"=>$v->messageId, 
								"uuid"=>$v->uuid
							);
						}

						$this->setError(NULL, NULL);
						$this->setSucess("eventos obtenidos", $arr);
					}
				}
			}
			else if( $this->isContactService() ) {
					if( $this->isExistNewContactData() ) {
						if( strstr($json->code, "duplicate") ) {
							$this->setError("contacto ya existe", "E01");
							$this->setSucess(NULL, NULL);
						}
						else {
							$this->setError(NULL, NULL);
							$this->setSucess("contactos", $json);
						}
					}
					else {
						$this->setError(NULL, NULL);
						$this->setSucess("contactos", $json);
					}
			}
			else {
				if( !isset($json->messageId) ) {
					$this->setError($json->message, $json->code);
				}
				else {
					$this->setError(NULL, NULL);
					$this->setSucess("transaccion exitosa", $json->messageId);
				}
			}
		}
	}

	/**
	* procesa el envio y llama el debugeo
	*/
	public function send() {
		if( !$this->user )	$this->setError("no indico el usuario para la conexion", "001");
		else if( !$this->key )	$this->setError("no indico el key para la conexion", "002");
		else if( !$this->subject )	$this->setError("no indico el asunto del correo", "003");
		else if( !$this->email_from )	$this->setError("no indico el correo del emisor", "004");
		else if( !is_array($this->email_to) || !count($this->email_to) )	$this->setError("no indico el correo del receptor", "005");
		else if( !$this->htmlBody )	$this->setError("no indico el contenido o cuerpo del correo", "006");
		else {
			$this->headers= array(
				"Accept-Encoding: gzip,deflate", 
				"Accept: application/json", 
				"User-Agent: sendinblueLight-PHP", 
				"Content-Type: application/json; charset=UTF-8", 
				"api-key: ". $this->getKey()
			);

			$d=array(
				"sender"=>array(
					"name"=>$this->getMailFromName(), 
					"email"=>$this->getMailFrom() 
				), 
				"to"=>$this->getMailTo(), /* obtenemos un array de correos */
				"replyTo"=>array(), 
				"bcc"=>array(), 
				"cc"=>array(), 
				"subject"=>$this->getSubject(), 
				"htmlContent"=>$this->getHtmlContent()
			);

			if( $this->isReplyToActive() ) {
				$d["replyTo"]= $this->getReplyToName();
			}
			else
				unset($d["replyTo"]);

			if( $this->isBccToActive() ) {
				$d["bcc"]= $this->getBccToName();
			}
			else
				unset($d["bcc"]);

			if( $this->isCcToActive() ) {
				$d["cc"]= $this->getCcToName();
			}
			else
				unset($d["cc"]);

			if( $this->isAttached() ) {
				$d["attachment"]= $this->getAttached();
			}

			$this->dataSend= json_encode($d); /* codificamos a json */
			unset($d);
			$this->sendToSendinblue();
		}
	}

	/**
	* devuelve los datos a mandar
	*/
	public function getDataSend() {
		return $this->dataSend;
	}

	/**
	* procesa la verificacion de estado de una transaccion anterior
	*/
	public function getStatus() {
		if( !$this->user )	$this->setError("no indico el usuario para la conexion", "001");
		else if( !$this->key )	$this->setError("no indico el key para la conexion", "002");
		else if( !$this->isTransaction() )	$this->setError("no existen transacciones registradas", "010");
		else {
			$this->headers= array(
				"Accept: application/json", 
				"User-Agent: sendinblueLight-PHP", 
				"Content-Type: application/json; charset=UTF-8", 
				"api-key: ". $this->getKey()
			);

			$this->sendToSendinblue();
		}
	}

	/**
	* establece el estado del servicio de consulta de contactos
	* 
	* @param boolean $a variable para activacion o desactivacion
	*/
	public function setContactService($a=NULL) {
		$this->contactService= ($a ? true:false);
	}

	/**
	* devuelve el estado de la variable del servicio de consulta de contactos
	*/
	public function getContactService() {
		return $this->contactService;
	}

	/**
	* verifica si el servicio de consulta de contactos esta activo o inactivo
	* 
	* @return boolean valos del estado del servicio
	*/
	public function isContactService() {
		return ($this->getContactService() ? true:false);
	}

	/**
	* establece informacion de la lista
	* 
	* @param intiger $idList identificador de la lista
	* @param intiger $limit valor del limite de consulta
	* @param intiger $offset valor del limite inicio
	* @param string $sort tipo de despliege asc o desc
	*/
	public function setList($idList=NULL, $limit=10, $offset=0, $sort="desc") {
		if( $this->isExistNewContactData() ) {
			# $r= '/lists/'.$idList.'/contacts/add';
			$r= '';
			$this->newContactIdList= $idList;
		}
		else {
			$r= '/lists'. ($idList ? '/'.$idList.'/contacts':''). '?limit='.$limit.'&offset='.$offset.'&sort='.$sort;
		}
		$this->setContactArgs($r);
		unset($r);
	}

	/**
	* devuelve el identificador de la lista
	* 
	* @return intiger identificador de la lista
	*/
	public function getListId() {
		return $this->newContactIdList;
	}

	/**
	* establece el nuevo nombre de contacto a crear
	* 
	* @param string $mail correo del contacto
	* @param string $name nombre del contacto
	*/
	public function setNewContact($mail=NULL, $name=NULL) {
		$this->newContactMail= ($mail ? $mail:NULL);
		$this->newContactMailName= ($name ? $name:NULL);
	}

	/**
	* verifica si existe contacto para guardarse
	* 
	* @return boolean validador de existencia
	*/
	public function isExistNewContactData() {
		return (count($this->newContactMail) ? true:false);
	}

	/**
	* devuelve la informacion del contacto
	* 
	* @param string $option tipo de informacion que requiere
	*/
	public function getNewContact($option=NULL) {
		if( !strcmp($option, "name") )
			return $this->newContactMailName;
		else
			return $this->newContactMail;
	}

	/**
	* guarda nuevo contacto
	*/
	public function addNewContact() {
		if( !$this->user )	$this->setError("no indico el usuario para la conexion", "001");
		else if( !$this->key )	$this->setError("no indico el key para la conexion", "002");
		else if( !$this->isExistNewContactData() )	$this->setError("no existen informacion del nuevo contacto", "012");
		else if( !$this->getListId() )	$this->setError("no indico un identificador de la lista", "013");
		else {
			$this->headers= array(
				"Accept: application/json", 
				"User-Agent: sendinblueLight-PHP", 
				"Content-Type: application/json; charset=UTF-8", 
				"api-key: ". $this->getKey()
			);

			$d= array("email"=>$this->getNewContact(), "listIds"=>array($this->getListId()) );
			$this->dataSend= json_encode($d); /* codificamos a json */
			$this->sendToSendinblue();
		}
	}	

	/**
	* obtiene la informacion de la lista
	*/
	public function getList() {
		if( !$this->user )	$this->setError("no indico el usuario para la conexion", "001");
		else if( !$this->key )	$this->setError("no indico el key para la conexion", "002");
		else if( !$this->getContactArgs() )	$this->setError("no existen argumentos para la busqueda de contactos", "011");
		else {
			$this->headers= array(
				"Accept: application/json", 
				"User-Agent: sendinblueLight-PHP", 
				"Content-Type: application/json; charset=UTF-8", 
				"api-key: ". $this->getKey()
			);

			$this->sendToSendinblue();
		}
	}

	/**
	* establece las variables para la consulta del contacto
	* 
	* @param string variables del argumento
	*/
	public function setContactArgs($a=NULL) {
		$this->contactArgs=($a ? $a:NULL);
	}

	/**
	* obtiene las variables para la consulta del contacto
	* 
	* @return string variables del argumento
	*/
	public function getContactArgs() {
		return $this->contactArgs;
	}

	/**
	* envia la informacion al api brevo.com
	*/
	public function sendToSendinblue() {
		if( !$this->user )	$this->setError("no indico el usuario para la conexion", "001");
		else if( !$this->key )	$this->setError("no indico el key para la conexion", "002");
		else if( !$this->headers )	$this->setError("no indico las cabeceras para enviar a sendinblue", "007");
		else if( !$this->isContactService() && !$this->isTransaction() && !$this->dataSend )	$this->setError("no indico los datos a enviar a sendinblue", "008");
		else {
			if( $this->isTransaction() ) {
				# ?limit=50&offset=0&messageId=
				foreach( $this->getTransaction() as $key=>$val ) {
					$s= curl_init();

					curl_setopt($s, CURLOPT_URL, self::API_SENDINBLUE_STATUS.'?limit=50&offset=0&messageId='.$val["id"] );
					curl_setopt($s, CURLOPT_HTTPHEADER, $this->headers );
					curl_setopt($s, CURLOPT_HEADER, 1 );
					curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($s, CURLOPT_VERBOSE, true);
					curl_setopt($s, CURLINFO_HEADER_OUT, true);
					$resp= curl_exec($s);
					$rq= curl_getinfo($s);
					$this->setHeaderRequest($rq["request_header"]); // request
					$this->setHeaderResponse($resp); // response
					$this->debugResponse();
					curl_close($s);
					unset($rq, $resp, $s);

					$eventos= $this->getResponse("array");

					foreach( $eventos["code"] as $k=>$v ) {
						$s= curl_init();
						curl_setopt($s, CURLOPT_URL, self::API_SENDINBLUE_STATUS.'/'.$v["uuid"] );
						curl_setopt($s, CURLOPT_HTTPHEADER, $this->headers );
						curl_setopt($s, CURLOPT_HEADER, 1 );
						curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($s, CURLOPT_VERBOSE, true);
						curl_setopt($s, CURLINFO_HEADER_OUT, true);
						$resp= curl_exec($s);
						$rq= curl_getinfo($s);
						$this->setHeaderRequest($rq["request_header"]); // request
						$this->setHeaderResponse($resp); // response
						$this->debugResponse();
						curl_close($s);
						unset($rq, $resp, $s);

						if( count($this->newListEvents) ) {
							$this->messageId["response"][$v["mail"]]= array( "sent"=>0, "delivered"=>0, "open"=>0 ); // init

							foreach( $this->newListEvents as $k1=>$v2 ) {
								$this->messageId["response"][$v["mail"]][$v2->name]= $v2->time;
							}
						}
					}
				}
			}
			else if( $this->isContactService() ) { # contactos
				$s= curl_init();

				curl_setopt($s, CURLOPT_URL, self::API_SENDINBLUE_CONTACTS.$this->getContactArgs() );
				curl_setopt($s, CURLOPT_HTTPHEADER, $this->headers );
				curl_setopt($s, CURLOPT_HEADER, 1 );
				curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($s, CURLOPT_VERBOSE, true);
				curl_setopt($s, CURLINFO_HEADER_OUT, true);

				if( $this->isExistNewContactData() ) {
					curl_setopt($s, CURLOPT_POST, 1);
					curl_setopt($s, CURLOPT_POSTFIELDS, $this->dataSend);
				}

				$resp= curl_exec($s);
				$rq= curl_getinfo($s);
				$this->setHeaderRequest($rq["request_header"]); // request
				$this->setHeaderResponse($resp); // response
				$this->debugResponse();
				curl_close($s);

				unset($rq, $resp, $s);
			}
			else { # normal
				$s= curl_init();

				curl_setopt($s, CURLOPT_URL, self::API_SENDINBLUE );
				curl_setopt($s, CURLOPT_HTTPHEADER, $this->headers );
				curl_setopt($s, CURLOPT_HEADER, 1 );
				curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($s, CURLOPT_VERBOSE, true);
				curl_setopt($s, CURLINFO_HEADER_OUT, true);
				curl_setopt($s, CURLOPT_POST, 1);
				curl_setopt($s, CURLOPT_POSTFIELDS, $this->dataSend);

				$resp= curl_exec($s);
				$rq= curl_getinfo($s);
				$this->setHeaderRequest($rq["request_header"]); // request
				$this->setHeaderResponse($resp); // response
				$this->debugResponse();
				curl_close($s);

				unset($rq, $resp, $s);
			}
		}
	}

	/**
	* establecemos el usuario o correo electronic
	*
	* @param string $a el usuario o correo
	*/
	public function setUser($a=NULL) {
		$this->user= ($a ? $a:(self::API_USER));
	}

	/**
	* retorna el usuario o correo electronic
	*
	* @return string el usuario o correo
	*/
	public function getUser($a=NULL) {
		return $this->user;
	}

	/**
	* establecemos la clave del api
	*
	* @param string $a la clave del token del api
	*/
	public function setKey($a=NULL) {
		$this->key= ($a ? $a:(self::API_KEY));
	}

	/**
	* retorna la clave del api
	*
	* @return string la clave del token del api
	*/
	public function getKey($a=NULL) {
		return $this->key;
	}

	/**
	* inicializa los dato para conexiones futuras
	*
	* @param string $user el usuario o correo
	* @param string $key el token del key api
	*/
	public function __construct($user=NULL, $key=NULL) {
		$this->setUser($user);
		$this->setKey($key);
	}
}
?>